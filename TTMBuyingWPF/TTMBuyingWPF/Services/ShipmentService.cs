﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTMBuyingWPF.DAL;
using TTMBuyingWPF.DAL.UnitOfWork;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace TTMBuyingWPF.Services
{
    public interface IShipmentService
    {
        void Create(string shipmentCode, string truckNo, DateTime createDate, string username);
        void Edit(string shipmentCode, string truckNo, DateTime createDate, string username);
        void Delete(string shipmentCode);
        void Finish(string shipmentCode, string username);
        void UnFinish(string shipmentCode, string username);
        Shipment GetSingle(string shipmentCode);
        List<Shipment> GetByYear(int year);
        List<Shipment> GetByDate(DateTime createDate);
    }

    public class ShipmentService : IShipmentService
    {
        IUnitOfWork _uow;

        public ShipmentService()
        {
            _uow = new UnitOfWork();
        }

        public void Create(string shipmentCode, string truckNo, DateTime createDate, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Truck no be empty.");

                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Create user cannot be empty.");

                var shipment = GetSingle(shipmentCode);
                if (shipment != null)
                    throw new Exception("Dupplicated shipment code");

                _uow.ShipmentRepository
                    .Add(new Shipment
                    {
                        ShipmentCode = shipmentCode,
                        TruckNo = truckNo,
                        CreateDate = createDate,
                        CreateBy = username,
                        IsFinish = false,
                        FinishDate = null,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = username,
                    });
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string shipmentCode)
        {
            try
            {
                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == shipmentCode
                    , x => x.Buyings);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                if (shipment.IsFinish == true)
                    throw new Exception("Shipment นี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว" +
                        " ไม่สามารถลบข้อมูลได้");

                if (shipment.Buyings.Count() > 0)
                    throw new Exception("มีสินค้าบันทึกใน shipment นี้ ไม่สามารถลบ shipment นี้ได้");
                _uow.ShipmentRepository
                    .Remove(shipment);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string shipmentCode, string truckNo, DateTime createDate, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == shipmentCode);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                if (shipment.IsFinish == true)
                    throw new Exception("Shipment นี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว" +
                        " ไม่สามารถลบข้อมูลได้");

                shipment.ShipmentCode = shipmentCode;
                shipment.TruckNo = truckNo;
                shipment.CreateDate = createDate;
                shipment.ModifiedBy = username;
                shipment.ModifiedDate = DateTime.Now;

                _uow.ShipmentRepository.Update(shipment);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string shipmentCode, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == shipmentCode);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                shipment.IsFinish = true;
                shipment.FinishDate = DateTime.Now;
                shipment.ModifiedDate = DateTime.Now;
                shipment.ModifiedBy = username;

                _uow.ShipmentRepository.Update(shipment);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Shipment> GetByDate(DateTime createDate)
        {
            return _uow.ShipmentRepository
                .Get(x => x.CreateDate.Day == createDate.Day &&
                x.CreateDate.Month == createDate.Month &&
                x.CreateDate.Year == createDate.Year
                , null
                , x => x.Buyings);
        }

        public List<Shipment> GetByYear(int year)
        {
            return _uow.ShipmentRepository
                .Get(x => x.CreateDate.Year == year
                , null
                , x => x.Buyings);
        }

        public Shipment GetSingle(string shipmentCode)
        {
            return _uow.ShipmentRepository
                .GetSingle(x => x.ShipmentCode == shipmentCode);
        }

        public void UnFinish(string shipmentCode, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == shipmentCode);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                shipment.IsFinish = false;
                shipment.ModifiedDate = DateTime.Now;
                shipment.ModifiedBy = username;

                _uow.ShipmentRepository.Update(shipment);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
