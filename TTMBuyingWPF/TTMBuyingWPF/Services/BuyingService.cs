﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using TTMBuyingWPF.DAL;
using TTMBuyingWPF.DAL.UnitOfWork;

namespace TTMBuyingWPF.Services
{
    public interface IBuyingService
    {
        void Add(string ttmBarcode, string shipmentCode, decimal weight, string username);
        void Edit(string ttmBarcode, string shipmentCode, decimal weight, string username);
        void Delete(string ttmBarcode);
        Buying GetSingle(string ttmBarcode);
        List<Buying> GetByShipmentCode(string shipmentCode);
    }

    public class BuyingService : IBuyingService
    {
        IUnitOfWork _uow;

        public BuyingService()
        {
            _uow = new UnitOfWork();
        }

        public void Add(string ttmBarcode, string shipmentCode, decimal weight, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(ttmBarcode))
                    throw new Exception("TTM barcode cannot be empty.");

                if (string.IsNullOrEmpty(shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                if (string.IsNullOrEmpty(username))
                    throw new Exception("Username cannot be empty.");

                if (weight <= 0)
                    throw new Exception("Weight must be more than zero.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == shipmentCode);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                if (shipment.IsFinish == true)
                    throw new Exception("Shipment นี้ถูก finish แล้ว.");

                var buying = _uow.BuyingRepository
                    .GetSingle(x => x.TTMBarcode == ttmBarcode);
                if (buying != null)
                    throw new Exception("มีบาร์โค้ตนี้แล้วในระบบ");

                _uow.BuyingRepository
                    .Add(new Buying
                    {
                        TTMBarcode = ttmBarcode,
                        Weight = weight,
                        ShipmentCode = shipmentCode,
                        RecordBy = username,
                        RecordDate = DateTime.Now,
                        ModifiedBy = username,
                        ModifiedDate = DateTime.Now
                    });
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string ttmBarcode)
        {
            try
            {
                if (string.IsNullOrEmpty(ttmBarcode))
                    throw new Exception("TTM barcode cannot be empty.");

                var buying = _uow.BuyingRepository
                    .GetSingle(x => x.TTMBarcode == ttmBarcode);
                if (buying == null)
                    throw new Exception("Find TTM barcode not found.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == buying.ShipmentCode);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                if (shipment.IsFinish == true)
                    throw new Exception("Shipment นี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว" +
                        " ไม่สามารถลบข้อมูลได้");

                _uow.BuyingRepository.Remove(buying);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string ttmBarcode, string shipmentCode, decimal weight, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(ttmBarcode))
                    throw new Exception("TTM barcode cannot be empty.");

                if (string.IsNullOrEmpty(username))
                    throw new Exception("Username cannot be empty.");

                if (weight <= 0)
                    throw new Exception("Weight must be more than zero.");

                var buying = _uow.BuyingRepository
                    .GetSingle(x => x.TTMBarcode == ttmBarcode);
                if (buying == null)
                    throw new Exception("Find TTM barcode not found.");

                var shipment = _uow.ShipmentRepository
                    .GetSingle(x => x.ShipmentCode == buying.ShipmentCode);
                if (shipment == null)
                    throw new Exception("Find shipment not found.");

                if (shipment.IsFinish == true)
                    throw new Exception("Shipment นี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว" +
                        " ไม่สามารถลบข้อมูลได้");

                buying.ShipmentCode = shipmentCode;
                buying.Weight = weight;
                buying.ModifiedDate = DateTime.Now;
                buying.ModifiedBy = username;
                _uow.BuyingRepository.Update(buying);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Buying> GetByShipmentCode(string shipmentCode)
        {
            return _uow.BuyingRepository
                .Get(x => x.ShipmentCode == shipmentCode);
        }

        public Buying GetSingle(string ttmBarcode)
        {
            return _uow.BuyingRepository
                .GetSingle(x => x.Equals(ttmBarcode));
        }
    }
}
