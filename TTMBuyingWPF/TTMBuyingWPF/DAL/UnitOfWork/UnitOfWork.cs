﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTMBuyingWPF.DAL.Repository;

namespace TTMBuyingWPF.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private bool _disposed = false;
        private string _errorMessage = string.Empty;

        private readonly TTMBuyingSystemEntities _context;
        private DbContextTransaction _transaction;
        public IGenericRepository<Buying> _buyingRepository;
        public IGenericRepository<Shipment> _shipmentRepository;

        public UnitOfWork()
        {
            _context = new TTMBuyingSystemEntities();
        }

        public IGenericRepository<Shipment> ShipmentRepository
        {
            get
            {
                return _shipmentRepository ?? (_shipmentRepository = new GenericRepository<Shipment>(_context));
            }
        }

        public IGenericRepository<Buying> BuyingRepository
        {
            get
            {
                return _buyingRepository ?? (_buyingRepository = new GenericRepository<Buying>(_context));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();
            _disposed = true;
        }

        public void CreateTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage = _errorMessage + $"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage} {Environment.NewLine}";
                    }
                }
                throw new Exception(_errorMessage, dbEx);
            }
        }
    }
}
