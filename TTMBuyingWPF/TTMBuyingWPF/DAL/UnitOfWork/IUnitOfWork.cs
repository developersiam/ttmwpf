﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTMBuyingWPF.DAL.Repository;

namespace TTMBuyingWPF.DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Shipment> ShipmentRepository { get; }
        IGenericRepository<Buying> BuyingRepository { get; }

        void CreateTransaction();

        void Commit();

        void Rollback();

        void Save();
    }
}
