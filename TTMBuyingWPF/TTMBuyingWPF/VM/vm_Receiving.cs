﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTMBuyingWPF.BL;
using TTMBuyingWPF.DAL;
using TTMBuyingWPF.MVVM;
using TTMBuyingWPF.Helper;
using System.Windows.Input;
using System.Windows;

namespace TTMBuyingWPF.VM
{
    public class vm_Receiving : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Receiving()
        {
            _recordDate = DateTime.Now.Date;
            RaisePropertyChangedEvent(nameof(RecordDate));
            OnFocusRequested(nameof(TTMBarcode));
            DataGridBinding();
        }

        #region Properties
        private TTMBuying _ttmBuying;

        public TTMBuying Buying
        {
            get { return _ttmBuying; }
            set { _ttmBuying = value; }
        }

        private DateTime _recordDate;

        public DateTime RecordDate
        {
            get { return _recordDate; }
            set
            {
                _recordDate = value;
                DataGridBinding();
            }
        }

        private string _ttmBarcode;

        public string TTMBarcode
        {
            get { return _ttmBarcode; }
            set { _ttmBarcode = value; }
        }

        private int _totalCases;

        public int TotalCases
        {
            get { return _totalCases; }
            set { _totalCases = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }
        #endregion

        #region List
        private List<TTMBuying> _ttmBuyingList;

        public List<TTMBuying> BuyingList
        {
            get { return _ttmBuyingList; }
            set { _ttmBuyingList = value; }
        }
        #endregion

        #region Command
        private ICommand _recordDateSelectedChangedCommand;

        public ICommand RecordDateSelectedChangedCommand
        {
            get { return _recordDateSelectedChangedCommand ?? (_recordDateSelectedChangedCommand = new RelayCommand(SelectedDateChanged)); }
            set { _recordDateSelectedChangedCommand = value; }
        }

        private void SelectedDateChanged(object obj)
        {
            DataGridBinding();
        }

        private ICommand _ttmBarcodeEnterCommand;

        public ICommand TTMBarcodeEnterCommand
        {
            get { return _ttmBarcodeEnterCommand ?? (_ttmBarcodeEnterCommand = new RelayCommand(TTMBarcodeEnter)); }
            set { _ttmBarcodeEnterCommand = value; }
        }

        private void TTMBarcodeEnter(object obj)
        {
            Save();
        }

        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }

        private void Save(object obj)
        {
            Save();
        }

        private ICommand _refreshCommand;

        public ICommand RefreshCommand
        {
            get { return _refreshCommand ?? (_refreshCommand = new RelayCommand(Refresh)); }
            set { _refreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            DataGridBinding();
        }

        private ICommand _dataGridDeleteCommand;

        public ICommand DataGridDeleteCommand
        {
            get { return _dataGridDeleteCommand ?? (_dataGridDeleteCommand = new RelayCommand(DataGridDeleteSelected)); }
            set { _dataGridDeleteCommand = value; }
        }

        private void DataGridDeleteSelected(object obj)
        {
            try
            {
                var item = (TTMBuying)obj;
                if (item == null)
                    return;

                ///Delete fucntion from BL.
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลการรับยานี้ใช่หรือไม่?") == MessageBoxResult.No) return;

                BLService.RecievingBL().Delete(item.TTMBarcode);
                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion

        #region Function
        private void DataGridBinding()
        {
            try
            {
                _ttmBuyingList = BLService.RecievingBL().GetByRecordDate(_recordDate);
                _totalCases = _ttmBuyingList.Count();
                _totalWeight = _ttmBuyingList.Sum(x => x.Weight);

                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(TotalCases));
                RaisePropertyChangedEvent(nameof(TotalWeight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save()
        {
            try
            {
                if (string.IsNullOrEmpty(_ttmBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(TTMBarcode));
                    return;
                }

                BLService.RecievingBL().Edit(_ttmBarcode);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            _ttmBarcode = "";
            RaisePropertyChangedEvent(nameof(TTMBarcode));
            OnFocusRequested(nameof(TTMBarcode));
        }
        #endregion
    }
}
