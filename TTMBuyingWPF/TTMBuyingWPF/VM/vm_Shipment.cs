﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TTMBuyingWPF.BL;
using TTMBuyingWPF.DAL;
using TTMBuyingWPF.Helper;
using TTMBuyingWPF.MVVM;

namespace TTMBuyingWPF.VM
{
    public class vm_Shipment : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Shipment()
        {
            _shipmentCode = BLService.ShipmentBL().GetNewShipmentCode();
            _createDate = DateTime.Now.Date;
            _machineName = Environment.MachineName;
            RaisePropertyChangedEvent(nameof(ShipmentCode));
            RaisePropertyChangedEvent(nameof(CreateDate));
            RaisePropertyChangedEvent(nameof(MachineName));
            DataGridBinding();
            ClearForm();
        }


        #region Properties
        private string _shipmentCode;

        public string ShipmentCode
        {
            get { return _shipmentCode; }
            set { _shipmentCode = value; }
        }

        private string _truckNo;

        public string TruckNo
        {
            get { return _truckNo; }
            set { _truckNo = value; }
        }

        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        private string _machineName;

        public string MachineName
        {
            get { return _machineName; }
            set { _machineName = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private Shipment _selectedItem;

        public Shipment SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }
        #endregion



        #region List
        private List<Shipment> _shipmentList;

        public List<Shipment> ShipmentList
        {
            get { return _shipmentList; }
            set { _shipmentList = value; }
        }
        #endregion


        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                BLService.ShipmentBL().Add(_shipmentCode, _truckNo, _createDate, _machineName);
                DataGridBinding();
                ClearForm();
                MessageBoxHelper.Info("Add new item is successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") ==
                    MessageBoxResult.No)
                    return;

                _selectedItem.TruckNo = _truckNo;
                _selectedItem.CreateDate = _createDate;
                _selectedItem.MachineName = _machineName;

                BLService.ShipmentBL()
                    .Edit(_selectedItem.ShipmentCode,
                    _selectedItem.TruckNo,
                    _selectedItem.CreateDate,
                    _machineName);
                DataGridBinding();
                ClearForm();
                MessageBoxHelper.Info("Edit item is successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                var item = (Shipment)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                BLService.ShipmentBL().Delete(item.ShipmentCode);
                DataGridBinding();
                ClearForm();
                MessageBoxHelper.Info("Delete item is successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var item = (Shipment)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _shipmentCode = item.ShipmentCode;
                _truckNo = item.TruckNo;
                _createDate = item.CreateDate;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _selectedItem = item;

                RaisePropertyChangedEvent(nameof(ShipmentCode));
                RaisePropertyChangedEvent(nameof(TruckNo));
                RaisePropertyChangedEvent(nameof(CreateDate));
                RaisePropertyChangedEvent(nameof(MachineName));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            try
            {
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _itemCodeEnterCommand;

        public ICommand ItemCodeEnterCommand
        {
            get { return _itemCodeEnterCommand ?? (_itemCodeEnterCommand = new RelayCommand(ItemCodeEnter)); }
            set { _itemCodeEnterCommand = value; }
        }

        private void ItemCodeEnter(object obj)
        {
            OnFocusRequested(nameof(TruckNo));
        }

        private ICommand _descriptionEnterCommand;

        public ICommand DescriptionEnterCommand
        {
            get { return _descriptionEnterCommand ?? (_descriptionEnterCommand = new RelayCommand(DescriptionEnter)); }
            set { _descriptionEnterCommand = value; }
        }

        private void DescriptionEnter(object obj)
        {
            OnFocusRequested(nameof(CreateDate));
        }
        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                _shipmentList = BLService.ShipmentBL()
                    .GetAll()
                    .OrderByDescending(x => x.ShipmentCode)
                    .ToList();
                _totalRecord = _shipmentList.Count();
                RaisePropertyChangedEvent(nameof(ShipmentList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _shipmentCode = BLService.ShipmentBL().GetNewShipmentCode();
            _truckNo = "";
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _selectedItem = null;

            RaisePropertyChangedEvent(nameof(ShipmentCode));
            RaisePropertyChangedEvent(nameof(TruckNo));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItem));

            OnFocusRequested(nameof(TruckNo));
        }
        #endregion
    }
}
