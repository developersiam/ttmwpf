﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TTMBuyingWPF.Helper;
using TTMBuyingWPF.DAL;
using System.Windows;
using TTMBuyingWPF.MVVM.Setting;

namespace TTMBuyingWPF.VM
{
    public class vm_MainWindow : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_MainWindow()
        {
            _recordDate = DateTime.Now.Date;
            RaisePropertyChangedEvent(nameof(RecordDate));
            OnFocusRequested(nameof(ShipmentCode));
            DataGridBinding();
            ShipmentListBinding();
        }


        #region Properties
        private Buying _buying;

        public Buying Buying
        {
            get { return _buying; }
            set { _buying = value; }
        }

        private DateTime _recordDate;

        public DateTime RecordDate
        {
            get { return _recordDate; }
            set
            {
                _recordDate = value;
                DataGridBinding();
            }
        }

        private string _shipmentCode;

        public string ShipmentCode
        {
            get { return _shipmentCode; }
            set
            {
                _shipmentCode = value;
                DataGridBinding();
                Clear();
            }
        }

        private string _ttmBarcode;

        public string TTMBarcode
        {
            get { return _ttmBarcode; }
            set { _ttmBarcode = value; }
        }

        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private int _totalCases;

        public int TotalCases
        {
            get { return _totalCases; }
            set { _totalCases = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }
        #endregion



        #region List
        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }

        private List<Shipment> _shipmentList;

        public List<Shipment> ShipmentList
        {
            get { return _shipmentList; }
            set { _shipmentList = value; }
        }
        #endregion



        #region Command
        private ICommand _recordDateSelectedChangedCommand;

        public ICommand RecordDateSelectedChangedCommand
        {
            get { return _recordDateSelectedChangedCommand ?? (_recordDateSelectedChangedCommand = new RelayCommand(SelectedDateChanged)); }
            set { _recordDateSelectedChangedCommand = value; }
        }

        private void SelectedDateChanged(object obj)
        {
            DataGridBinding();
        }

        private ICommand _ttmBarcodeEnterCommand;

        public ICommand TTMBarcodeEnterCommand
        {
            get { return _ttmBarcodeEnterCommand ?? (_ttmBarcodeEnterCommand = new RelayCommand(TTMBarcodeEnter)); }
            set { _ttmBarcodeEnterCommand = value; }
        }

        private void TTMBarcodeEnter(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_ttmBarcode))
                    throw new ArgumentException("โปรดสแกนรหัสบาร์โค้ต");

                _buying = BLService.BuyingBL().GetSingle(_ttmBarcode);

                if (_buying != null)
                {
                    _weight = _buying.Weight;
                    _recordDate = _buying.RecordDate.Date;
                    _shipmentCode = _buying.ShipmentCode;

                    RaisePropertyChangedEvent(nameof(Weight));
                    RaisePropertyChangedEvent(nameof(RecordDate));
                    RaisePropertyChangedEvent(nameof(ShipmentCode));
                }

                RaisePropertyChangedEvent(nameof(Weight));
                OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _weightEnterCommand;

        public ICommand WeightEnterCommand
        {
            get { return _weightEnterCommand ?? (_weightEnterCommand = new RelayCommand(WeightEnter)); }
            set { _weightEnterCommand = value; }
        }

        private void WeightEnter(object obj)
        {
            Save();
        }

        private ICommand _weightGotFocusCommand;

        public ICommand WeightGotFocusCommand
        {
            get { return _weightGotFocusCommand ?? (_weightGotFocusCommand = new RelayCommand(WeightGotFocus)); }
            set { _weightGotFocusCommand = value; }
        }

        private void WeightGotFocus(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุทะเบียนรถ");
                    OnFocusRequested(nameof(ShipmentCode));
                    return;
                }

                if (string.IsNullOrEmpty(_ttmBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ตก่อน");
                    OnFocusRequested(nameof(TTMBarcode));
                    return;
                }

                var window = new Numpad();
                var vm = new vm_Numpad();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                _weight = vm.Weight;
                RaisePropertyChangedEvent(nameof(Weight));
                OnFocusRequested(nameof(Weight));
                Save();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }

        private void Save(object obj)
        {
            Save();
        }

        private ICommand _clearCommand;

        public ICommand ClearCommand
        {
            get { return _clearCommand ?? (_clearCommand = new RelayCommand(Clear)); }
            set { _clearCommand = value; }
        }

        private void Clear(object obj)
        {
            Clear();
        }

        private ICommand _refreshCommand;

        public ICommand RefreshCommand
        {
            get { return _refreshCommand ?? (_refreshCommand = new RelayCommand(Refresh)); }
            set { _refreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            DataGridBinding();
        }

        private ICommand _exportCommand;

        public ICommand ExportCommand
        {
            get { return _exportCommand ?? (_exportCommand = new RelayCommand(Export)); }
            set { _exportCommand = value; }
        }

        private void Export(object obj)
        {
            ExcelExport();
        }

        private ICommand _shipmentCommand;

        public ICommand ShipmentCommand
        {
            get { return _shipmentCommand ?? (_shipmentCommand = new RelayCommand(ManageShipment)); }
            set { _shipmentCommand = value; }
        }

        private void ManageShipment(object obj)
        {
            try
            {
                var window = new Form.Shipment();
                var vm = new vm_Shipment();
                window.DataContext = vm;
                window.ShowDialog();
                ShipmentListBinding();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _dataGridEditCommand;

        public ICommand DataGridEditCommand
        {
            get { return _dataGridEditCommand ?? (_dataGridEditCommand = new RelayCommand(DataGridEditSelected)); }
            set { _dataGridEditCommand = value; }
        }

        private void DataGridEditSelected(object obj)
        {
            try
            {
                var item = (Buying)obj;
                if (item == null)
                    return;

                _buying = item;
                _ttmBarcode = item.TTMBarcode;
                _weight = item.Weight;
                _shipmentCode = item.ShipmentCode;
                _recordDate = item.RecordDate;

                RaisePropertyChangedEvent(nameof(TTMBarcode));
                RaisePropertyChangedEvent(nameof(Weight));
                RaisePropertyChangedEvent(nameof(ShipmentCode));
                RaisePropertyChangedEvent(nameof(RecordDate));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _dataGridDeleteCommand;

        public ICommand DataGridDeleteCommand
        {
            get { return _dataGridDeleteCommand ?? (_dataGridDeleteCommand = new RelayCommand(DataGridDeleteSelected)); }
            set { _dataGridDeleteCommand = value; }
        }

        private void DataGridDeleteSelected(object obj)
        {
            try
            {
                var item = (Buying)obj;
                if (item == null)
                    return;

                ///Delete fucntion from BL.
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BLService.BuyingBL().Delete(item.TTMBarcode);
                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DataGridBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                    _buyingList = new List<Buying>();

                _buyingList = BLService.BuyingBL().GetByShipment(_shipmentCode);
                _totalCases = _buyingList.Count();
                _totalWeight = _buyingList.Sum(x => x.Weight);

                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(TotalCases));
                RaisePropertyChangedEvent(nameof(TotalWeight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save()
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Shipment Code");
                    OnFocusRequested(nameof(ShipmentCode));
                    return;
                }

                if (string.IsNullOrEmpty(_ttmBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(TTMBarcode));
                    return;
                }

                if (_weight <= 0)
                {
                    MessageBoxHelper.Warning("น้ำหนักไม่ควรเป็น 0");
                    OnFocusRequested(nameof(Weight));
                }

                if (_buying == null)
                    BLService.BuyingBL().Add(_ttmBarcode, _weight, _shipmentCode, _recordDate);
                else
                {
                    if (MessageBoxHelper.Question("ป้ายบาร์โค้ตนี้ซ้ำในระบบ" + Environment.NewLine +
                        Environment.NewLine +
                        "TTM Barcode  " + _ttmBarcode + Environment.NewLine +
                        "Record Date: " + _buying.RecordDate.Date + Environment.NewLine +
                        "Modified Date: " + _buying.ModifiedDate + Environment.NewLine +
                        "Shipment Code: " + _buying.ShipmentCode + Environment.NewLine +
                        Environment.NewLine +
                        "ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                        return;
                    BLService.BuyingBL().Edit(_ttmBarcode, _weight, _shipmentCode, _recordDate);
                }

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            _ttmBarcode = "";
            _weight = (decimal)0.00;
            _buying = null;
            RaisePropertyChangedEvent(nameof(TTMBarcode));
            RaisePropertyChangedEvent(nameof(Weight));
            RaisePropertyChangedEvent(nameof(Buying));
            OnFocusRequested(nameof(TTMBarcode));
        }

        private void ExcelExport()
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                {
                    MessageBoxHelper.Warning("กรุณาระบุ shipment code");
                    OnFocusRequested(nameof(ShipmentCode));
                    return;
                }

                _buyingList = BLService.BuyingBL().GetByShipment(_shipmentCode);

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog(); //

                //if (result == System.Windows.Forms.DialogResult.OK)
                //{
                //    string folderPath = ExportHelper.ExportBuying(folderDlg.SelectedPath, _recordDate, _buyingList);
                //    Environment.SpecialFolder root = folderDlg.RootFolder;
                //    MessageBoxHelper.Info("Export Complete.");
                //    System.Diagnostics.Process.Start(folderPath);
                //}
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShipmentListBinding()
        {
            _shipmentList = BLService.ShipmentBL()
                .GetAll()
                .OrderByDescending(x => x.ShipmentCode)
                .ToList();
            RaisePropertyChangedEvent(nameof(ShipmentList));
        }
        #endregion
    }
}
