﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TTMBuyingWPF.Helper;
using TTMBuyingWPF.MVVM;

namespace TTMBuyingWPF.VM
{
    public class vm_Numpad : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Numpad()
        {
            _weightText = "";
            RaisePropertyChangedEvent(nameof(WeightText));
        }


        #region Properties
        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private string _weightText;

        public string WeightText
        {
            get { return _weightText; }
            set { _weightText = value; }
        }

        private Numpad _window;

        public Numpad Window
        {
            get { return _window; }
            set { _window = value; }
        }

        #endregion



        #region Command
        private ICommand _num1Command;

        public ICommand Num1Command
        {
            get { return _num1Command ?? (_num1Command = new RelayCommand(Num1Press)); }
            set { _num1Command = value; }
        }

        private void Num1Press(object obj)
        {
            ConcateWeight("1");
        }

        private ICommand _num2Command;

        public ICommand Num2Command
        {
            get { return _num2Command ?? (_num2Command = new RelayCommand(Num2Press)); }
            set { _num2Command = value; }
        }

        private void Num2Press(object obj)
        {
            ConcateWeight("2");
        }

        private ICommand _num3Command;

        public ICommand Num3Command
        {
            get { return _num3Command ?? (_num3Command = new RelayCommand(Num3Press)); }
            set { _num3Command = value; }
        }

        private void Num3Press(object obj)
        {
            ConcateWeight("3");
        }

        private ICommand _num4Command;

        public ICommand Num4Command
        {
            get { return _num4Command ?? (_num4Command = new RelayCommand(Num4Press)); }
            set { _num4Command = value; }
        }

        private void Num4Press(object obj)
        {
            ConcateWeight("4");
        }

        private ICommand _num5Command;

        public ICommand Num5Command
        {
            get { return _num5Command ?? (_num5Command = new RelayCommand(Num5Press)); }
            set { _num5Command = value; }
        }

        private void Num5Press(object obj)
        {
            ConcateWeight("5");
        }

        private ICommand _num6Command;

        public ICommand Num6Command
        {
            get { return _num6Command ?? (_num6Command = new RelayCommand(Num6Press)); }
            set { _num6Command = value; }
        }

        private void Num6Press(object obj)
        {
            ConcateWeight("6");
        }

        private ICommand _num7Command;

        public ICommand Num7Command
        {
            get { return _num7Command ?? (_num7Command = new RelayCommand(Num7Press)); }
            set { _num7Command = value; }
        }

        private void Num7Press(object obj)
        {
            ConcateWeight("7");
        }

        private ICommand _num8Command;

        public ICommand Num8Command
        {
            get { return _num8Command ?? (_num8Command = new RelayCommand(Num8Press)); }
            set { _num8Command = value; }
        }

        private void Num8Press(object obj)
        {
            ConcateWeight("8");
        }

        private ICommand _num9Command;

        public ICommand Num9Command
        {
            get { return _num9Command ?? (_num9Command = new RelayCommand(Num9Press)); }
            set { _num9Command = value; }
        }

        private void Num9Press(object obj)
        {
            ConcateWeight("9");
        }

        private ICommand _num0Command;

        public ICommand Num0Command
        {
            get { return _num0Command ?? (_num0Command = new RelayCommand(Num0Press)); }
            set { _num0Command = value; }
        }

        private void Num0Press(object obj)
        {
            ConcateWeight("0");
        }

        private ICommand _numDotCommand;

        public ICommand NumDotCommand
        {
            get { return _numDotCommand ?? (_numDotCommand = new RelayCommand(NumDotPress)); }
            set { _numDotCommand = value; }
        }

        private void NumDotPress(object obj)
        {
            ConcateWeight(".");
        }

        private ICommand _okCommand;

        public ICommand OKCommand
        {
            get { return _okCommand ?? (_okCommand = new RelayCommand(OK)); }
            set { _okCommand = value; }
        }

        private void OK(object obj)
        {
            if(Helper.RegularExpressionHelper.IsDecimalCharacter(_weightText) == false)
            {
                MessageBoxHelper.Warning("รูปแบบตัวเลขทศนิยมไม่ถูกต้อง โปรดตรวจสอบอีกครั้ง");
                return;
            }

            _weight = Convert.ToDecimal(_weightText);
            _window.Close();
        }

        private ICommand _clearCommand;

        public ICommand ClearCommand
        {
            get { return _clearCommand ?? (_clearCommand = new RelayCommand(Clear)); }
            set { _clearCommand = value; }
        }

        private void Clear(object obj)
        {
            _weightText = "";
            _weight = (decimal)0.00;
            RaisePropertyChangedEvent(nameof(Weight));
            RaisePropertyChangedEvent(nameof(WeightText));
        }

        #endregion



        #region Function
        private void ConcateWeight(string num)
        {
            _weightText = _weightText.ToString() + num;
            RaisePropertyChangedEvent(nameof(WeightText));
        }
        #endregion
    }
}
