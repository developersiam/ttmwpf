﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using TTMBuyingWPF.DAL;

namespace TTMBuyingWPF.Helper
{
    public static class ExportHelper
    {
        public static string ExportBuying(string folderName, DateTime exportDate, List<Buying> _buyingList)
        {
            string name = "" + exportDate.ToString("dd-MM-yyyy");
            //name = name.Replace("/", "-");
            string pathName = @"" + folderName + "\\" + name + ".xlsx";
            FileInfo info = new FileInfo(pathName);

            if (info.Exists) File.Delete(pathName);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(name);

                ws.Cells["A1"].LoadFromCollection(_buyingList, true);
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();

                ws.Cells["A1"].Value = "TTMBarcode";
                ws.Cells["B1"].Value = "Weight";
                ws.Cells["C1"].Value = "TruckNo";
                ws.Cells["D1"].Value = "RecordDate";
                ws.Cells["E1"].Value = "ModifiedDate";

                using (ExcelRange col = ws.Cells[2, 4, 2 + _buyingList.Count, 5]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 5, 2 + _buyingList.Count, 6]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }
            return @"" + folderName;
        }
    }
}
