﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TTMBuyingWPF.Helper
{
    public static class RegularExpressionHelper
    {
        public static bool IsDecimalCharacter(string str)
        {
            Regex rgx = new Regex(@"^([0-9.]+)$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
        public static bool IsNumericCharacter(string str)
        {
            Regex rgx = new Regex(@"^([0-9]+)$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
        public static bool IsBaleBarcodeCorrectFormat(string str)
        {
            Regex rgx = new Regex(@"^([0-9.]+)$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
        public static bool IsContractCodeCorrectFormat(string str)
        {
            Regex rgx = new Regex(@"^([a-zA-Z0-9-]+)$");
            if (!rgx.IsMatch(str))
                return false;
            else
                return true;
        }
    }
}
