﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TTMBuyingWPF.MVVM;
using TTMBuyingWPF.VM;

namespace TTMBuyingWPF.Form
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Page
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new vm_MainWindow();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_MainWindow)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.ShipmentCode):
                    ShipmentComboBox.Focus();
                    break;
                case nameof(vm.TTMBarcode):
                    TTMBarcodeTextBox.SelectAll();
                    TTMBarcodeTextBox.Focus();
                    break;
                case nameof(vm.Weight):
                    WeightTextBox.SelectAll();
                    WeightTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
