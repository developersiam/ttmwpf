﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TTMBuyingWPF.MVVM;
using TTMBuyingWPF.VM;

namespace TTMBuyingWPF.Form
{
    /// <summary>
    /// Interaction logic for Recieve.xaml
    /// </summary>
    public partial class Recieve : Page
    {
        public Recieve()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = new vm_Receiving();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Receiving)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.RecordDate):
                    RecordDateDatePicker.Focus();
                    break;
                case nameof(vm.TTMBarcode):
                    TTMBarcodeTextBox.SelectAll();
                    TTMBarcodeTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
