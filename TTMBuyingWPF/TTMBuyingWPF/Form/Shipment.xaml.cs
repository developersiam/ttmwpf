﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TTMBuyingWPF.MVVM;
using TTMBuyingWPF.VM;

namespace TTMBuyingWPF.Form
{
    /// <summary>
    /// Interaction logic for Shipment.xaml
    /// </summary>
    public partial class Shipment : Window
    {
        public Shipment()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Shipment)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.CreateDate):
                    CreateDateDatePicker.Focus();
                    break;
                case nameof(vm.TruckNo):
                    TruckNoTextBox.SelectAll();
                    TruckNoTextBox.Focus();
                    break;
                case nameof(vm.ShipmentCode):
                    ShipmentCodeTextBox.SelectAll();
                    ShipmentCodeTextBox.Focus();
                    break;
                default:
                    break;
            }
        }

    }
}
