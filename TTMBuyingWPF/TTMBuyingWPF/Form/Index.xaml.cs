﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TTMBuyingWPF.Form
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Index : Window
    {
        public Index()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void RecieveMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Recieve());
        }

        private void BuyingMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new MainWindow());
        }
    }
}
