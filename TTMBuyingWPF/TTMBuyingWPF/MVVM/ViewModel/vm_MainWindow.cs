﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TTMBuyingWPF.MVVM.Setting;
using TTMBuyingWPF.MVVM.View;

namespace TTMBuyingWPF.MVVM.ViewModel
{
    public class vm_MainWindow : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_MainWindow() { }

        #region Properties
        private Frame _frame;

        public Frame Frame
        {
            get { return _frame; }
            set { _frame = value; }
        }
        #endregion


        #region Command
        private ICommand _onHomeMenuClickCommand;

        public ICommand OnHomeMenuClickCommand
        {
            get { return _onHomeMenuClickCommand ?? (_onHomeMenuClickCommand = new RelayCommand(OnHomeMenuClick)); }
            set { _onHomeMenuClickCommand = value; }
        }

        private void OnHomeMenuClick(object obj)
        {
            _frame.NavigationService.Navigate(new Home());
        }

        private ICommand _onShipmentMenuClickCommand;

        public ICommand OnShipmentMenuClickCommand
        {
            get { return _onShipmentMenuClickCommand ?? (_onShipmentMenuClickCommand = new RelayCommand(OnShipmentMenuClick)); }
            set { _onShipmentMenuClickCommand = value; }
        }

        private void OnShipmentMenuClick(object obj)
        {
            var page = new Shipment();
            var vm = new vm_Shipment();
            page.DataContext = vm;
            _frame.NavigationService.Navigate(page);
        }

        private ICommand _onReportMenuClickCommand;

        public ICommand OnReportMenuClickCommand
        {
            get { return _onReportMenuClickCommand ?? (_onReportMenuClickCommand = new RelayCommand(OnReportMenuClick)); }
            set { _onReportMenuClickCommand = value; }
        }

        private void OnReportMenuClick(object obj)
        {
            _frame.NavigationService.Navigate(new Report());
        }
        #endregion
    }
}
