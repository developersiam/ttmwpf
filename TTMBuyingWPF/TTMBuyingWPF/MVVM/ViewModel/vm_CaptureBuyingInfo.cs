﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml;
using TTMBuyingWPF.DAL;
using TTMBuyingWPF.Helper;
using TTMBuyingWPF.MVVM.Setting;

namespace TTMBuyingWPF.MVVM.ViewModel
{
    public class vm_CaptureBuyingInfo : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_CaptureBuyingInfo()
        { }

        #region Properties  
        private string _shipmentCode;

        public string ShipmentCode
        {
            get { return _shipmentCode; }
            set
            {
                _shipmentCode = value;
                ShipmentInfoBinding();
                BuyingListBinding();
            }
        }

        private string _truckNo;

        public string TruckNo
        {
            get { return _truckNo; }
            set { _truckNo = value; }
        }

        private DateTime _createShipmentDate;

        public DateTime CreateShipmentDate
        {
            get { return _createShipmentDate; }
            set { _createShipmentDate = value; }
        }

        private bool _shipmentStatus;

        public bool ShipmentStatus
        {
            get { return _shipmentStatus; }
            set { _shipmentStatus = value; }
        }

        private DateTime? _shipmentFinishDate;

        public DateTime? ShipmentFinishDate
        {
            get { return _shipmentFinishDate; }
            set { _shipmentFinishDate = value; }
        }

        private string _ttmBarcode;

        public string TTMBarcode
        {
            get { return _ttmBarcode; }
            set { _ttmBarcode = value; }
        }

        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private int _totalCartons;

        public int TotalCartons
        {
            get { return _totalCartons; }
            set { _totalCartons = value; }
        }

        private double _totalWeight;

        public double TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private Buying _selectedEditItem;

        public Buying SelectedEditItem
        {
            get { return _selectedEditItem; }
            set { _selectedEditItem = value; }
        }

        #endregion


        #region List
        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }
        #endregion


        #region Command
        private ICommand _onBarcodeEnterCommand;

        public ICommand OnBarcodeEnterCommand
        {
            get { return _onBarcodeEnterCommand ?? (_onBarcodeEnterCommand = new RelayCommand(OnBarcodeEnter)); }
            set { _onBarcodeEnterCommand = value; }
        }

        private void OnBarcodeEnter(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_ttmBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนหรือพิมพ์รหัสบาร์โค้ต");
                    OnFocusRequested(nameof(TTMBarcode));
                    return;
                }
                OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onBarcodeGotFocusCommand;

        public ICommand OnBarcodeGorFocusCommand
        {
            get { return _onBarcodeGotFocusCommand ?? (_onBarcodeGotFocusCommand = new RelayCommand(OnBarcodeGorFocus)); }
            set { _onBarcodeGotFocusCommand = value; }
        }

        private void OnBarcodeGorFocus(object obj)
        {
            Clear();
        }

        private ICommand _onWeightEnterCommand;

        public ICommand OnWeightEnterCommand
        {
            get { return _onWeightEnterCommand ?? (_onWeightEnterCommand = new RelayCommand(OnWeightEnter)); }
            set { _onWeightEnterCommand = value; }
        }

        private void OnWeightEnter(object obj)
        {
            Save();
        }

        private ICommand _onSaveButtonClickCommand;

        public ICommand OnSaveButtonClickCommand
        {
            get { return _onSaveButtonClickCommand ?? (_onSaveButtonClickCommand = new RelayCommand(OnSaveButtonClick)); }
            set { _onSaveButtonClickCommand = value; }
        }

        private void OnSaveButtonClick(object obj)
        {
            Save();
        }

        private ICommand _onClearButtonClickCommand;

        public ICommand OnClearButtonClickCommand
        {
            get { return _onClearButtonClickCommand ?? (_onClearButtonClickCommand = new RelayCommand(OnClearButtonClick)); }
            set { _onClearButtonClickCommand = value; }
        }

        private void OnClearButtonClick(object obj)
        {
            Clear();
        }

        private ICommand _onRefreshButtonClickCommand;

        public ICommand OnRefreshButtonClickCommand
        {
            get { return _onRefreshButtonClickCommand ?? (_onRefreshButtonClickCommand = new RelayCommand(OnRefreshButtonClick)); }
            set { _onRefreshButtonClickCommand = value; }
        }

        private void OnRefreshButtonClick(object obj)
        {
            ShipmentInfoBinding();
            BuyingListBinding();
            Clear();
        }

        private ICommand _onRowDeleteButtonClickCommand;

        public ICommand OnRowDeleteButtonClickCommand
        {
            get { return _onRowDeleteButtonClickCommand ?? (_onRowDeleteButtonClickCommand = new RelayCommand(OnRowDeleteButtonClick)); }
            set { _onRowDeleteButtonClickCommand = value; }
        }

        private void OnRowDeleteButtonClick(object obj)
        {
            try
            {
                var item = (Buying)obj;
                if (item == null)
                    return;

                if (_shipmentStatus == true)
                    throw new Exception("Shipment code นี้ถูก Finish" +
                        " แล้วไม่สามารถเปลี่ยนแปลงข้อมูลได้");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                Services.Services.BuyingService()
                    .Delete(item.TTMBarcode);
                BuyingListBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRowEditButtonClickCommand;

        public ICommand OnRowEditButtonClickCommand
        {
            get { return _onRowEditButtonClickCommand ?? (_onRowEditButtonClickCommand = new RelayCommand(OnRowEditButtonClick)); }
            set { _onRowEditButtonClickCommand = value; }
        }

        private void OnRowEditButtonClick(object obj)
        {
            try
            {
                _selectedEditItem = (Buying)obj;
                if (_selectedEditItem == null)
                    return;

                _ttmBarcode = _selectedEditItem.TTMBarcode;
                _weight = _selectedEditItem.Weight;
                RaisePropertyChangedEvent(nameof(TTMBarcode));
                RaisePropertyChangedEvent(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onFinishButtonClickCommand;

        public ICommand OnFinishButtonClickCommand
        {
            get { return _onFinishButtonClickCommand ?? (_onFinishButtonClickCommand = new RelayCommand(OnFinishButtonClick)); }
            set { _onFinishButtonClickCommand = value; }
        }

        private void OnFinishButtonClick(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                if (MessageBoxHelper.Question("ท่านต้องการล็อค shipment นี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                if (_buyingList.Count() <= 0)
                    if (MessageBoxHelper.Question("ไม่พบข้อมูลสินค้าที่ถูกบันทึกอยู่ภายใน shipment นี้" +
                        " ท่านต้องการล็อค shipment นี้ใช่หรือไม่?")
                        == System.Windows.MessageBoxResult.No)
                        return;

                Services.Services.ShipmentService()
                    .Finish(_shipmentCode, Environment.MachineName);
                ShipmentInfoBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnfinishButtonClickCommand;

        public ICommand OnUnfinishButtonClickCommand
        {
            get { return _onUnfinishButtonClickCommand ?? (_onUnfinishButtonClickCommand = new RelayCommand(OnUnfinishButtonClick)); }
            set { _onUnfinishButtonClickCommand = value; }
        }

        private void OnUnfinishButtonClick(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                    throw new Exception("Shipment code cannot be empty.");

                if (MessageBoxHelper.Question("ท่านต้องการปลดล็อค shipment นี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                Services.Services.ShipmentService()
                    .UnFinish(_shipmentCode, Environment.MachineName);
                ShipmentInfoBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void ShipmentInfoBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                    throw new Exception("ไม่พบ shipment code นี้ในระบบ ไม่สามารถบันทึกข้อมูลสินค้าได้");

                var shipment = Services.Services.ShipmentService()
                    .GetSingle(_shipmentCode);
                if (shipment == null)
                    throw new Exception("ไม่พบ shipment code นี้ในระบบ ไม่สามารถบันทึกข้อมูลสินค้าได้");

                _truckNo = shipment.TruckNo;
                _createShipmentDate = shipment.CreateDate;
                _shipmentStatus = shipment.IsFinish;
                _shipmentFinishDate = shipment.FinishDate;
                RaisePropertyChangedEvent(nameof(TruckNo));
                RaisePropertyChangedEvent(nameof(CreateShipmentDate));
                RaisePropertyChangedEvent(nameof(ShipmentStatus));
                RaisePropertyChangedEvent(nameof(ShipmentFinishDate));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BuyingListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                    throw new Exception("ไม่สามารถแสดงรายการข้อมูลได้เนื่องจากไม่พบ shipment code");

                _buyingList = Services.Services.BuyingService()
                    .GetByShipmentCode(_shipmentCode)
                    .OrderByDescending(x => x.ModifiedDate)
                    .ToList();
                _totalCartons = _buyingList.Count();
                _totalWeight = (double)_buyingList.Sum(x => x.Weight);
                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(TotalCartons));
                RaisePropertyChangedEvent(nameof(TotalWeight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            try
            {
                _ttmBarcode = string.Empty;
                _weight = 0;
                _selectedEditItem = null;
                RaisePropertyChangedEvent(nameof(TTMBarcode));
                RaisePropertyChangedEvent(nameof(Weight));
                RaisePropertyChangedEvent(nameof(SelectedEditItem));
                OnFocusRequested(nameof(TTMBarcode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save()
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                    throw new Exception("ไม่พบข้อมูล shipment code");

                if (string.IsNullOrEmpty(_ttmBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนหรือพิมพ์บาร์โค้ตลงในช่อง TTM Barcode");
                    OnFocusRequested(nameof(TTMBarcode));
                    return;
                }

                if (_weight <= 0)
                {
                    MessageBoxHelper.Warning("น้ำหนักสินค้าจะต้องมากกว่า 0");
                    OnFocusRequested(nameof(Weight));
                    return;
                }

                if (_shipmentStatus == true)
                    throw new Exception("Shipment code นี้ถูก Finish" +
                        " แล้วไม่สามารถเปลี่ยนแปลงข้อมูลได้");

                if (_selectedEditItem == null)
                {
                    Services.Services.BuyingService()
                        .Add(_ttmBarcode,
                        _shipmentCode,
                        _weight,
                        Environment.MachineName);
                }
                else
                {
                    if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?")
                        == System.Windows.MessageBoxResult.No)
                        return;

                    Services.Services.BuyingService()
                        .Edit(_selectedEditItem.TTMBarcode,
                        _shipmentCode,
                        _weight,
                        Environment.MachineName);
                }

                BuyingListBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
