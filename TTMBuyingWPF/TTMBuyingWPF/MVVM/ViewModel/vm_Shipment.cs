﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TTMBuyingWPF.DAL;
using TTMBuyingWPF.Helper;
using TTMBuyingWPF.MVVM.Setting;
using TTMBuyingWPF.MVVM.View;
using TTMBuyingWPF.Services;

namespace TTMBuyingWPF.MVVM.ViewModel
{
    public class vm_Shipment : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Shipment()
        {
            _createDate = DateTime.Now;
            ShipmentListBinding();
            RaisePropertyChangedEvent(nameof(CreateDate));
        }

        #region Properties
        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                _createDate = value;
                ShipmentListBinding();
                RaisePropertyChangedEvent(nameof(CreateDate));
            }
        }

        private string _truckNo;

        public string TruckNo
        {
            get { return _truckNo; }
            set { _truckNo = value; }
        }

        private string _shipmentCode;

        public string ShipmentCode
        {
            get { return _shipmentCode; }
            set { _shipmentCode = value; }
        }

        private int _totalShipment;

        public int TotalShipment
        {
            get { return _totalShipment; }
            set { _totalShipment = value; }
        }

        private int _totalCartons;

        public int TotalCartons
        {
            get { return _totalCartons; }
            set { _totalCartons = value; }
        }

        private double _totalWeight;

        public double TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private ShipmentSummary _selectedShipment;

        public ShipmentSummary SelectedShipment
        {
            get { return _selectedShipment; }
            set { _selectedShipment = value; }
        }

        private bool _isEditMode;

        public bool IsEditMode
        {
            get { return _isEditMode; }
            set { _isEditMode = value; }
        }

        #endregion


        #region List
        private List<ShipmentSummary> _shipmentList;

        public List<ShipmentSummary> ShipmentList
        {
            get { return _shipmentList; }
            set { _shipmentList = value; }
        }
        #endregion


        #region Command
        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                var shipment = (ShipmentSummary)obj;
                if (shipment == null)
                    return;

                if (shipment.TotalCartons > 0)
                    throw new Exception("ภายใน shipment มีการบันทึกข้อมูลสินค้าอยู่จำนวน " +
                        shipment.TotalCartons.ToString("N0") + " กล่อง ไม่สามารถลบได้");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                Services.Services.ShipmentService().Delete(shipment.ShipmentCode);
                ShipmentListBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                if (obj == null)
                    return;
                var shipment = (ShipmentSummary)obj;
                _selectedShipment = shipment;
                _shipmentCode = shipment.ShipmentCode;
                _truckNo = shipment.TruckNo;
                _isEditMode = true;
                RaisePropertyChangedEvent(nameof(ShipmentCode));
                RaisePropertyChangedEvent(nameof(TruckNo));
                RaisePropertyChangedEvent(nameof(IsEditMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCreateCommand;

        public ICommand OnCreateCommand
        {
            get { return _onCreateCommand ?? (_onCreateCommand = new RelayCommand(OnCreate)); }
            set { _onCreateCommand = value; }
        }

        private void OnCreate(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_shipmentCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ shipment code");
                    OnFocusRequested(nameof(ShipmentCode));
                    return;
                }

                if (string.IsNullOrEmpty(_truckNo))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Truck no");
                    OnFocusRequested(nameof(TruckNo));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการสร้าง Shipment ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                Services.Services.ShipmentService()
                    .Create(_shipmentCode,
                    _truckNo,
                    _createDate,
                    Environment.MachineName);
                ShipmentListBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (_selectedShipment == null)
                    throw new Exception("โปรดเลือกรายการ shipment ที่ต้องการแก้ไขได้จากตารางด้านล่าง");

                if (string.IsNullOrEmpty(_shipmentCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ shipment code");
                    OnFocusRequested(nameof(ShipmentCode));
                    return;
                }

                if (string.IsNullOrEmpty(_truckNo))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Truck no");
                    OnFocusRequested(nameof(TruckNo));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไข Shipment นี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                Services.Services.ShipmentService()
                    .Edit(_selectedShipment.ShipmentCode,
                    _truckNo,
                    _createDate,
                    Environment.MachineName);
                ShipmentListBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            Clear();
        }

        private ICommand _onDataGridMouseDoubleClickCommand;

        public ICommand OnDataGridMouseDoubleClickCommand
        {
            get { return _onDataGridMouseDoubleClickCommand ?? (_onDataGridMouseDoubleClickCommand = new RelayCommand(OnDataGridMouseDoubleClick)); }
            set { _onDataGridMouseDoubleClickCommand = value; }
        }

        private void OnDataGridMouseDoubleClick(object obj)
        {
            var shipment = (ShipmentSummary)obj;
            if (shipment == null)
                return;
            var window = new CaptureBuyingInfo();
            var vm = new vm_CaptureBuyingInfo();
            window.DataContext = vm;
            vm.ShipmentCode = shipment.ShipmentCode;
            window.ShowDialog();
            ShipmentListBinding();
        }
        #endregion


        #region Function
        public void ShipmentListBinding()
        {
            try
            {
                _shipmentList = Services.Services.ShipmentService()
                      .GetByDate(_createDate)
                      .Select(x => new ShipmentSummary
                      {
                          ShipmentCode = x.ShipmentCode,
                          TruckNo = x.TruckNo,
                          CreateDate = x.CreateDate,
                          CreateBy = x.CreateBy,
                          IsFinish = x.IsFinish,
                          FinishDate = x.FinishDate,
                          ModifiedBy = x.ModifiedBy,
                          ModifiedDate = x.ModifiedDate,
                          TotalCartons = x.Buyings.Count,
                          TotalWeight = (double)x.Buyings.Sum(y => y.Weight)
                      })
                      .ToList();
                _totalShipment = _shipmentList.Count;
                _totalCartons = _shipmentList.Sum(x => x.TotalCartons);
                _totalWeight = _shipmentList.Sum(x => x.TotalWeight);

                RaisePropertyChangedEvent(nameof(ShipmentList));
                RaisePropertyChangedEvent(nameof(TotalShipment));
                RaisePropertyChangedEvent(nameof(TotalCartons));
                RaisePropertyChangedEvent(nameof(TotalWeight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Clear()
        {
            try
            {
                _shipmentCode = string.Empty;
                _truckNo = string.Empty;
                _selectedShipment = null;
                _isEditMode = false;

                RaisePropertyChangedEvent(nameof(ShipmentCode));
                RaisePropertyChangedEvent(nameof(TruckNo));
                RaisePropertyChangedEvent(nameof(SelectedShipment));
                RaisePropertyChangedEvent(nameof(IsEditMode));
                OnFocusRequested(nameof(ShipmentCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Classes
        public class ShipmentSummary : DAL.Shipment
        {
            public int TotalCartons { get; set; }
            public double TotalWeight { get; set; }
        }
        #endregion
    }
}
